TARGET = programm
CC ?= gcc

SRC = $(wildcard *.c)

all: $(TARGET)
	
$(TARGET) : $(SRC)
	$(CC) -Wall -Wextra -Wpedantic -std=c11 -g $(SRC) -o $(TARGET)

clean :
	rm $(TARGET)