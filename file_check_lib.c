#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "file_check_lib.h"

/* Добавление сигнатуры в конец */
sign_list *add_sign(sign_list *sl_begin, uint64_t sign, int32_t count)
{
    sign_list *sl_p;
    sign_list *new_sign = (sign_list *) malloc(sizeof(sign_list));
    new_sign->sign = sign;
    new_sign->count = count;
    if (sl_begin != NULL)
    {
        for (sl_p = sl_begin; sl_p->next != NULL; sl_p = sl_p->next);
        sl_p->next = new_sign;
    }
    else
    {
        sl_begin = new_sign;
    }
    return sl_begin;
}

/* Вставка сигнатуры после указанного адреса */
sign_list *insert_sign(sign_list *sl_addr, uint64_t sign, int32_t count)
{
    sign_list *new_sign = (sign_list *) malloc(sizeof(sign_list));
    new_sign->sign = sign;
    new_sign->count = count;
    if (sl_addr != NULL)
    {
        sign_list *next = sl_addr->next;
        sl_addr->next = new_sign;   
        new_sign->next = next;
        return sl_addr;
    }
    else
    {
        new_sign->next = NULL;
        return new_sign;
    }

}

/* Поиск номера элемента, начинающего сигнатуру */
sign_list *found_signature(FILE *rarjpg, char *file_name)
{
    sign_list *sl_begin = NULL;                 /* Список сигнатур, содержащихся в файле */
    sign_list *open_jpg = NULL;                 /* Указатель на сигнатуру начала jpg файла */
    sign_list *open_png = NULL;                 /* Указатель на сигнатуру начала png файла */
    char sign[8];                               /* Сигнатура, считываемая из файла */
    uint32_t count = 0;                         /* Номер элемента */
    struct stat info_file;                      /* Структура, содержащая информацию о файле */

    /* Считывание информации о файле */
    stat ((const char *) file_name, &info_file);

    /* Цикличное считывание из файла check_signature_size байт пока не дойдём до конца или не найдём иинтересующую нас сигнатуру */
    for (int i = 0; i < info_file.st_size; i++)
    {   
        if (i == 0)
        {
            fread(sign, 1, MAX_SIZE_SIGNATURE_IMAGE_BYTE, rarjpg);
            
            /* Проверка на сигнатуру начала jpg */
            if (*((uint16_t *) sign) == JPG_SIGNATURE_BEGIN)
            {
                sl_begin = add_sign(sl_begin, JPG_SIGNATURE_BEGIN, count);
                open_jpg = sl_begin;
            }

            /* Проверка на сигнатуру начала png */
            if (*((uint64_t *) sign) == PNG_SIGNATURE_BEGIN)
            {
                sl_begin = add_sign(sl_begin, PNG_SIGNATURE_BEGIN, count);
                open_png = sl_begin;
            }
        }
        else
        {
            memcpy(sign, sign + 1, MAX_SIZE_SIGNATURE_IMAGE_BYTE - 1);
            fread(sign + MAX_SIZE_SIGNATURE_IMAGE_BYTE - 1, 1, 1, rarjpg);
        }

        /* Проверка на сигнатуру конца jpg */
        if (open_jpg != NULL && *((uint16_t *) sign) == JPG_SIGNATURE_END)
        {
            sl_begin = add_sign(sl_begin, JPG_SIGNATURE_END, count);
            open_jpg = NULL;
        }

        /* Проверка на сигнатуру конца png */
        if (open_png != NULL && *((uint64_t *) sign) == PNG_SIGNATURE_END)
        {
            sl_begin = add_sign(sl_begin, PNG_SIGNATURE_END, count);
            open_png = NULL;
        }
        
        /* Проверка на сигнатуру конца центральной директории zip файла */
        if (*((uint32_t *) sign) == ZIP_CENTRAL_DIRECTORY_SIGNATURE_END)
        {
            sl_begin = add_sign(sl_begin, ZIP_CENTRAL_DIRECTORY_SIGNATURE_END, count);
        }
        count += 1;
    }

    if (open_jpg != NULL)
    {
        sl_begin = delete_sign(sl_begin, open_jpg);
    }

    if (open_png != NULL)
    {
        sl_begin = delete_sign(sl_begin, open_png);
    }

    return sl_begin;
}

/* Проверка: является ли файл изображением (JPG или PNG) */
uint8_t file_format(sign_list *sl_begin)
{
    uint8_t type = UNKNOWN_FILE;
    uint8_t jpg_sign = 0;
    uint8_t png_sign = 0;
    uint8_t zip_sign = 0;

    for (sign_list *p = sl_begin; p != NULL; p = p->next)
    {
        if(p->sign == (uint64_t) JPG_SIGNATURE_BEGIN && jpg_sign == 0)
        {
            jpg_sign = 1;
        }
        if(p->sign == (uint64_t) PNG_SIGNATURE_BEGIN && png_sign == 0)
        {
            png_sign = 1;
        }
        if(p->sign == (uint64_t) ZIP_CENTRAL_DIRECTORY_SIGNATURE_END && zip_sign == 0)
        {
            zip_sign = 1;
        }
    }

    if (jpg_sign == 1)
    {
        type = JPG;
    }

    if (png_sign == 1)
    {
        type = PNG;
    }

    if (zip_sign == 1)
    {
        switch (type)
        {
        case JPG: type = JPG_WITH_ZIP; break;
        case PNG: type = PNG_WITH_ZIP; break;
        case UNKNOWN_FILE: type = ZIP; break;
        
        default: break;
        }
    }


    return type;
}

/* Вывод информации по zip файлу */
void info_zip(FILE *rarjpg, sign_list *sl_begin)
{
    sign_list *sl_p = sl_begin;     /* Указатель для перемещения по списку */
    uint32_t addr_zip_begin = 0;    /* Номер элемента, с которого начинает zip файл */
    zip_central_end zce;            /* Структура конеца центрального заголовка */
    zip_central zc;                 /* Структура центральный заголовка файла */

    /* Поиск начала zip файла в jpg файле */
    if(sl_p->sign == JPG_SIGNATURE_BEGIN)
    {
        while(sl_p != NULL)
        {
            if(sl_p->sign != JPG_SIGNATURE_END)
            {
                sl_p = sl_p->next;
            }
            else
            {
                addr_zip_begin = sl_p->count + JPG_SIGNATURE_SIZE;
                break;
            }
        }
    }

    /* Поиск начала zip файла в png файле */
    if(sl_p->sign == PNG_SIGNATURE_BEGIN)
    {
        while(sl_p != NULL)
        {
            if(sl_p->sign != PNG_SIGNATURE_END)
            {
                sl_p = sl_p->next;
            }
            else
            {
                addr_zip_begin = sl_p->count + PNG_SIGNATURE_SIZE;
                break;
            }
        }
    }

    /* Поиск информации о сигнатуре конца центрального заголовка */
    while(sl_p != NULL)
    {
        if(sl_p->sign != ZIP_CENTRAL_DIRECTORY_SIGNATURE_END)
        {
            sl_p = sl_p->next;
        }
        else
        {
            break;
        }
    }

    if (sl_p == NULL)
    {
        printf("No zip arxive!!!\n");
        return;
    }

    /* Считываем конец центрального заголовка и находим смещение до начала центрального заголовка */
    fseek(rarjpg, sl_p->count, SEEK_SET);
    fread(&zce, 1, sizeof(zip_central_end), rarjpg);

    /* Считываем центральные заголовки файлов и выводим названия файлов */
    fseek(rarjpg, zce.cd_offset + addr_zip_begin, SEEK_SET);
    printf("\nFile in zip:\n");
    for (int i = 0; i < zce.cd_entries; i++)
    {
        char *file_name;

        fread(&zc, 1, sizeof(zip_central), rarjpg);
        file_name = (char *) malloc(zc.name_len + 1);
        fread(file_name, 1, zc.name_len, rarjpg);
        file_name[zc.name_len] = '\0';
        puts(file_name);
        free(file_name);
        fseek(rarjpg, zc.extra_len + zc.comment_len, SEEK_CUR);
    }
}

/* Удаление сигнатуры из списка */
sign_list *delete_sign(sign_list *sl_begin, sign_list *delete)
{
    sign_list *sl_p;
    if (sl_begin == delete)
    {
        sl_begin = sl_begin->next;
        free(delete);
    }
    else 
    {
        for(sl_p = sl_begin; sl_p->next != delete; sl_p = sl_p->next);
        sl_p->next = delete->next;
        free(delete);
    }
    return sl_begin;
}

/* Очистка списка сигнатур */
void free_sign_list(sign_list *sl_begin)
{
    for (;sl_begin != NULL;)
    {
        sign_list *next = sl_begin->next;
        free(sl_begin);
        sl_begin = next;
    }
}