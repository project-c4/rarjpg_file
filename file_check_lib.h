#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef FILE_CHECK_LIB_H
#define FILE_CHECK_LIB_H

/* Размеры сигнатур файлов */
#define JPG_SIGNATURE_SIZE  2               /* Размер сигнатур jpg файлов */
#define PNG_SIGNATURE_SIZE  8               /* Размер сигнатур png файлов */
#define ZIP_SIGNATURE_SIZE  4               /* Размер сигнатур zip файлов */
#define MAX_SIZE_SIGNATURE_IMAGE_BYTE 8     /* Максимальный размер сигнатур файлов */

/* Количество формата файлов изображений */
#define N_IMAGE_TYPE 2

/* Типы файлов */
#define UNKNOWN_FILE  0       /* Незнакомый файл */
#define JPG 100               /* jpg файл */
#define PNG 101               /* png файл */
#define ZIP 102               /* zip файл */
#define JPG_WITH_ZIP  202     /* Сшитый файл jpg + zip */
#define PNG_WITH_ZIP  203     /* Сшитый файл png + zip */


/* Сигнатуры файлов */
#define JPG_SIGNATURE_BEGIN                     0xD8FF                  /* Начальная сигнатура jpg файла */
#define JPG_SIGNATURE_END                       0xD9FF                  /* Конечная сигнатура jpg файла */
#define PNG_SIGNATURE_BEGIN                     0x0A1A0A0D474E5089      /* Начальная сигнатура png файла */
#define PNG_SIGNATURE_END                       0x826042AE444E4549      /* Конечная сигнатура png файла */
#define ZIP_ORDINARY_SIGNATURE                  0x04034B50              /* Сигнатура обычного zip файла*/
#define ZIP_EMPTY_SIGNATURE                     0x06054B50              /* Сигнатура пустого zip файла */
#define ZIP_DIVIDED_SIGNATURE                   0x08074B50              /* Сигнатура разделённого zip файла */
#define ZIP_CENTRAL_DIRECTORY_SIGNATURE_BEGIN   0x02014b50              /* Сигнатура центральной директории zip файла */
#define ZIP_CENTRAL_DIRECTORY_SIGNATURE_END     0x06054b50              /* Сигнатура конца центральной директории zip файла */

#pragma pack(push, 1)
typedef struct sign_list        /* Список сигнатур */
{
    uint64_t sign;              /* Сигнатура */
    uint32_t count;             /* Начало сигнатуры */
    struct sign_list *next;     /* Следующая сигнатура файла */
}sign_list;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct zip_central_end  /* Структура для считывания конца центрального заголовка */
{
    uint32_t signature;         /* Сигнатура коонца центрального заголовка */
    uint16_t disk_nbr;          /* Номер этого диска */
    uint16_t cd_start_disk;     /* Диск, на котором начинается центральный каталог */
    uint16_t disk_cd_entries;   /* Количество записей центрального каталога на этом диске */
    uint16_t cd_entries;        /* Общее количество записей центрального каталога */
    uint32_t cd_size;           /* Размер центрального каталога (байт) */
    uint32_t cd_offset;         /* Смещение начала центрального каталога относительно начала архива */
    uint16_t comment_len;       /* Длина комментария */
    const uint8_t *comment;     /* Комментарий */
}zip_central_end;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct zip_central      /* Структура для считывания центрального заголовка файла */
{
    uint32_t signature;         /* Сигнатура центрального заголовка */
    uint16_t made_by_ver;       /* Версия, созданная */
    uint16_t extract_ver;       /* Версия, необходимая для извлечения (минимальная) */
    uint16_t gp_flag;           /* Битовый флаг общего назначения */
    uint16_t method;            /* Метод сжатия */
    uint16_t mod_time;          /* Время последнего изменения файла */
    uint16_t mod_date;          /* Дата последнего изменения файла */
    uint32_t crc32;             /* CRC-32 несжатых данных */
    uint32_t comp_size;         /* Сжатый размер */
    uint32_t uncomp_size;       /* Несжатый размер */
    uint16_t name_len;          /* Длина имени файла */
    uint16_t extra_len;         /* Дополнительная длина поля */
    uint16_t comment_len;       /* Длина комментария к файлу */
    uint16_t disk_nbr_start;    /* Номер диска, с которого начинается файл */
    uint16_t int_attrs;         /* Внутренние атрибуты файла */
    uint32_t ext_attrs;         /* Внешние атрибуты файла */
    uint32_t lfh_offset;        /* Относительное смещение заголовка локального файла */
}zip_central;
#pragma pack(pop)

/* Добавление сигнатуры в конец */
sign_list *add_sign(sign_list *sl_begin, uint64_t sign, int32_t count);

/* Вставка сигнатуры после указанного элемента */
sign_list *insert_sign(sign_list *sl_addr, uint64_t sign, int32_t count);

/* Поиск номера элемента, начинающего сигнатуру */
sign_list *found_signature(FILE *rarjpg, char *file_name);

/* Проверка: является ли файл изображением (JPG или PNG) */
uint8_t file_format(sign_list *sl_begin);

/* Проверка: есть ли прикреплённный zip-файл */
uint8_t check_for_zip_arxiv(FILE *rarjpg);

/* Вывод информации по zip файлу */
void info_zip(FILE *rarjpg, sign_list *sl_begin);

/* Удаление сигнатуры из списка */
sign_list *delete_sign(sign_list *sl_begin, sign_list *delete);

/* Очистка списка сигнатур */
void free_sign_list(sign_list *sl_begin);

#endif /* FILE_CHECK_LIB_H*/
