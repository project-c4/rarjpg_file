#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "file_check_lib.h"

#define NO_OPEN_FILE 100
#define NO_INPUT_FILE 101

/* Вывод типа файла */
void print_type(uint8_t type)
{
    switch (type)
    {
    case JPG: printf("File type: jpg\n"); break;
    case PNG: printf("File type: png\n"); break;
    case ZIP: printf("File type: zip\n"); break;
    case JPG_WITH_ZIP: printf("File type: jpg with zip\n"); break;
    case PNG_WITH_ZIP: printf("File type: png with zip\n"); break;
    
    default: printf("File type: Unknown file\n"); break;
    }
}

int main(int argc, char *argv[])
{
    char *file_name;                /* Имя файла */
    FILE *rarjpg;                   /* Указатель на файл */
    uint8_t type;                   /* Тип файла */
    sign_list *sl_begin = NULL;     /* Начало списка сигнатур файла */

    if (argc == 1)
    {
        printf("Error %d!!! No input file!!!\n", NO_INPUT_FILE);
        return NO_INPUT_FILE;
    }
    /* Считываем имя файла */
    file_name = (char *) malloc(strlen(argv[1]));
    strcpy(file_name, argv[1]);

    /* Открываем файл для считывания по байтам */
    if ((rarjpg = fopen(file_name, "rb")) == NULL)
    {
        printf("Error %d!!! No open file!!!\n", NO_OPEN_FILE);
        return NO_OPEN_FILE;
    }

    /* Определяем формат файла */
    sl_begin = found_signature(rarjpg, file_name);
    type = file_format(sl_begin);
    print_type(type);

    /* Вывод содержимого zip файла */
    if (type == ZIP || type == JPG_WITH_ZIP || type == PNG_WITH_ZIP)
    {
        info_zip(rarjpg, sl_begin);
    }

    free_sign_list(sl_begin);
    free(file_name);
    fclose(rarjpg);
    return 0;
}
